```
sudo apt install avrdude
sudo apt --purge remove modemmanager
sudo usermod -a -G dialout $USER

avrdude -p m32U4 -P /dev/ttyACM0 -c avr109 -U eeprom:w:eeprom-lefthand.eep
avrdude -p m32U4 -P /dev/ttyACM0 -c avr109 -U eeprom:w:eeprom-righthand.eep

make lets_split/rev2:robt:avrdude

avrdude -p m32U4 -P /dev/ttyACM0 -c avr109 -U flash:w:lets_split_rev2_rob.hex

```

https://github.com/qmk/qmk_firmware/blob/3ac9259742bfd428f71c31dbf2bfedace2a7f91b/keyboards/lets_split/readme.md#setting-ee_hands-to-use-either-hands-as-master
